package com.example.pusher.lesson_helper;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import com.example.pusher.lesson_helper.Tag_db.DatabaseHelper;
import com.freesoulapps.preview.android.Preview;
import com.example.pusher.lesson_helper.Homepage;

import java.util.ArrayList;
import java.util.Arrays;

//This is page is setting about share to other social platform. Example FB.

public class Share_page extends AppCompatActivity {

    Preview web_preview;
    TableLayout tl_preview;
    TableRow tr_preview;
    String url;
    Toolbar toolbar;
    DatabaseHelper DB_Helper = new DatabaseHelper(this);
    ArrayList<String> tags = new ArrayList<>();
    Intent intent = new Intent();
    EditText edit_case_key;
    //Bundle bundle = new Bundle();

    //接下來要寫，增加標籤，的功能！
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_page);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        edit_case_key = (EditText)findViewById(R.id.edit_case_key);

        toolbar.inflateMenu(R.menu.menu_share_page);
        toolbar.setTitle("備課小幫手");

        web_preview = (Preview)findViewById(R.id.preview);
        tl_preview = (TableLayout)findViewById(R.id.tl_preview);
        tl_preview.setStretchAllColumns(true);
        tr_preview = (TableRow)findViewById(R.id.tr_preview);
        //tr_preview.setBackgroundColor(Color.rgb(153,255,153));
        url = getIntent().getStringExtra(Intent.EXTRA_TEXT);
        web_preview.setData(url);

        boolean add_new_tag = false;
        if(DB_Helper.get_tag().size()==0) {
            DB_Helper.add_tag("班級經營");
            DB_Helper.add_tag("教學素材");
            DB_Helper.add_tag("合作學習");
        }

        if(DB_Helper.get_tag().size()>0) {
            for (int i = 0; i < DB_Helper.get_tag().size(); i++) {
                tags.add(DB_Helper.get_tag().get(i).toString());
            }
            if(!add_new_tag) {
                tags.add("新增標籤");
                add_new_tag = true;
            }
        }
        //tags.add("");


        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.add_tag:
                        new AlertDialog.Builder(Share_page.this)
                                .setItems(tags.toArray(new String[tags.size()]), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        String name = tags.get(which);
                                        String key = edit_case_key.getText().toString();
                                        if(which != tags.size()-1) {
                                            Toast.makeText(getApplicationContext(), "Tag is" + name + which, Toast.LENGTH_SHORT).show();
                                            DB_Helper.add_case(name, key + "'s case");
                                            intent.setClass(Share_page.this, Homepage.class);
                                            startActivity(intent);
                                            finish();
                                        }else if(which == tags.size()-1) {
                                            final View item_view =  LayoutInflater.from(Share_page.this).inflate(R.layout.item_layout, null);
                                            new AlertDialog.Builder(Share_page.this)
                                                    .setTitle("新增標籤")
                                                    .setView(item_view)
                                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            EditText editText = (EditText) item_view.findViewById(R.id.edit_tag);
                                                            tags.add(tags.size()-1,editText.getText().toString());
                                                            DB_Helper.add_tag(editText.getText().toString());
                                                            Toast.makeText(getApplicationContext(), editText.getText().toString(), Toast.LENGTH_SHORT).show();
                                                        }
                                                    })
                                                    .show();
                                        }
                                    }
                                })
                                .show();
                        Log.e("tag", "is pressed.");
                        return true;
                    default:
                        return false;
                }
            }
        });

    }

    public void open_web(View v){
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        intent.setClass(Share_page.this, web_page.class);
        bundle.putString("url", url);

        intent.putExtras(bundle);

        startActivity(intent);
    }


}
