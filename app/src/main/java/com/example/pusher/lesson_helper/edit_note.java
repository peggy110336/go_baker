package com.example.pusher.lesson_helper;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pusher.lesson_helper.Tag_db.DatabaseHelper;

public class edit_note extends AppCompatActivity {

    DatabaseHelper DB_Helper = new DatabaseHelper(this);
    EditText edit_note_text;
    Bundle bundle;
    Toolbar toolbar;
    ActionBar bar;
    String case_title;
    int case_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);
        //toolbar = (Toolbar)findViewById(R.id.toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //toolbar.inflateMenu(R.menu.menu_edit_note);
        //toolbar.setTitle("備課小幫手");

        bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        //bar.setIcon(R.drawable.ic_action_save);
        bundle = getIntent().getExtras();
        case_title = bundle.getString("Title");
        case_id = bundle.getInt("ID");
        String note;

        edit_note_text = (EditText) findViewById(R.id.edit_notetext);
        note = DB_Helper.get_note(case_title, case_id);

        if(!note.equals("none")) {
            edit_note_text.setText(note);
        }


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_edit_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                // About option clicked.
                return true;
            case R.id.save_note:
                // Exit option clicked.
                DB_Helper.save_note(case_title, case_id, edit_note_text.getText().toString());
                Toast.makeText(edit_note.this, "筆記儲存成功！", Toast.LENGTH_SHORT).show();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
