package com.example.pusher.lesson_helper;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuItem;
import com.freesoulapps.preview.android.Preview;

/**
 * Created by kpeggy on 2016/4/9.
 */
public class ShareActivity extends Activity {
    ShareActionProvider share_provider;

    public ShareActivity(MenuItem shareItem) {
        share_provider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);

        setShareIntent(setIntent());
    }

    private Intent setIntent(){

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.setType("text/plan");
        sendIntent.putExtra(Intent.EXTRA_TEXT, sendIntent.getStringExtra(Intent.EXTRA_TEXT));


        return sendIntent;

    }

    private void setShareIntent(Intent shareIntent) {
        if (share_provider != null) {
            share_provider.setShareIntent(shareIntent);
        }
    }

}
