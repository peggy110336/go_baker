package com.example.pusher.lesson_helper;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by rick.wu on 2015/8/26.
 * fixed by K-Peggy on 2016/04/06.
 */
public class SearchableActivity extends Activity {
    TextView item_title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pager_item1);
        //item_title = (TextView)findViewById(R.id.item_title);
        handleIntent(getIntent());
    }
    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            //use the query to search your data somehow
            //item_title.setText(query);
        }
    }
}
