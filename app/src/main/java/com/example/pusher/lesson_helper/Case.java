package com.example.pusher.lesson_helper;

/**
 * Created by kpeggy on 2016/4/15.
 */
public class Case {
    private int id;
    private String name;
    private String tag;
    public Case() {
    }
    public Case(int id, String productname) {
        this.id = id;
        this.name = productname;
    }
    public Case(String productname) {
        this.name = productname;
    }
    public void setID(int id) {
        this.id = id;
    }
    public int getID() {
        return this.id;
    }
    public void setCaseTitle(String productname) {
        this.name = productname;
    }
    public String getCaseTitle() {
        return this.name;
    }
    public void setCaseTag(String producttag) {
        this.tag = producttag;
    }
    public String getCaseTag() {
        return this.tag;
    }
}
