package com.example.pusher.lesson_helper.Tag_db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.pusher.lesson_helper.Case;

import java.util.ArrayList;

import static com.example.pusher.lesson_helper.Tag_db.DBConstants.*;
import static android.provider.BaseColumns._ID;

/**
 * SQLite database open helper.
 *
 * @author Jun Gu (http://2dxgujun.com)
 * @version 1.0
 * @since 2015-2-5 20:19:29
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private final static String DB_NAME = "Case";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        /**db.execSQL("CREATE TABLE " + TagsTable.TABLE_NAME
                + "("
                + TagsTable._ID + " INTEGER PRIMARY KEY,"
                + TagsTable.TAG + " TEXT"
                + ");");**/

        final String TEST_CASE_TABLE = "CREATE TABLE IF NOT EXISTS "+ TABLE_CASE +
                " (" +
                DBConstants._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TAG + " VARCHER(16), " +
                CASE_TITLE + " VARCHER(32), " +
                STORE_URL + " VARCHER(128)," +
                NOTE + " VARCHER(128)" +
                ");";

        final String TEST_TAG_TABLE = "CREATE TABLE IF NOT EXISTS "+ TABLE_TAG +
                " (" +
                DBConstants._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                ADD_TAG + " VARCHER(16)" +
                ");";

        db.execSQL(TEST_CASE_TABLE);
        db.execSQL(TEST_TAG_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_CASE); //刪除舊有的資料表
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_TAG); //刪除舊有的資料表
        onCreate(db);
    }

    public void add_case(String tag,String title) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TAG, tag);
        values.put(CASE_TITLE, title);
        //values.put(TAG, tag);
        db.insert(TABLE_CASE, null, values);
        Log.e("DB", values.toString());
        db.close();
    }

    public void add_tag(String tag) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ADD_TAG, tag);
        //values.put(TAG, tag);
        db.insert(TABLE_TAG, null, values);
        Log.e("DB", values.toString());
        db.close();
    }

    public ArrayList<String> get_tag(){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<String> string_tag = new ArrayList<String>();
        String query = "Select * FROM " + TABLE_TAG;
        Cursor cursor = db.rawQuery(query, null);
        int rows_num = cursor.getCount();
        if(rows_num != 0) {
            cursor.moveToFirst();
            for(int i=0; i<rows_num; i++) {
                string_tag.add(cursor.getString(1));
                cursor.moveToNext();
            }
        }
        cursor.close();
        return string_tag;
    }

    public boolean save_note(String title,int id,String note){
        SQLiteDatabase db = this.getWritableDatabase();
        //String query = "UPDATE '" + TABLE_CASE + "'('" + NOTE + "') VALUES('" + note + "') WHERE " + _ID + "=" + id +";";
        String query = "UPDATE '" + TABLE_CASE + "' SET '" + NOTE + "' = '" + note + "' WHERE " + _ID + "=" + id +";";
        db.execSQL(query);

        return true;
    }
    public String get_note(String title,int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "Select * FROM " + TABLE_CASE + " WHERE " + _ID + "=" + id +";";
        Cursor cursor = db.rawQuery(query,null);
        int rows_num = cursor.getCount();
        cursor.moveToFirst();

        if(!cursor.isNull(4)) {
            Log.e("note", cursor.getString(4).toString());
            return cursor.getString(4).toString();
        }
        else
            return "none";
    }

    public ArrayList<Case> get_AllCase_num(){
        SQLiteDatabase db = this.getWritableDatabase();
        ArrayList<Case> cases = new ArrayList<Case>();
        String query = "Select * FROM " + TABLE_CASE;
        Cursor cursor = db.rawQuery(query, null);
        int rows_num = cursor.getCount();
        if(rows_num != 0) {
            cursor.moveToFirst();
            for(int i=0; i<rows_num; i++) {
                Case case_example = new Case();
                case_example.setID(Integer.parseInt(cursor.getString(0)));
                case_example.setCaseTag(cursor.getString(1));
                case_example.setCaseTitle(cursor.getString(2));
                cases.add(case_example);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return cases;
    }
}