package com.example.pusher.lesson_helper.Tag_db;

import android.provider.BaseColumns;

/**
 * Created by kpeggy on 2016/4/14.
 */
public class DBConstants implements BaseColumns {
    //public static final String TABLE_NAME_USER_PROFILE = "user_profile";
    //    public static final String TABLE_NAME_CLASS_LIST = "class_list";
    public static final String TABLE_CASE = "case_table";
    public static final String TABLE_TAG = "tag_table";


    public static final String NOTE = "note";
    public static final String STORE_URL = "store_url";
    public static final String CASE_TITLE = "title";
    public static final String TAG = "case_tag";
    public static final String ADD_TAG = "add_tag";
}
