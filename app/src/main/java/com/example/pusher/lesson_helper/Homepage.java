package com.example.pusher.lesson_helper;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
//import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pusher.lesson_helper.Tag_db.DatabaseHelper;
import com.freesoulapps.preview.android.Preview;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.List;


// fixed by K-Peggy on 2016/04/06.

public class Homepage extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;

    private ListView listDrawer;

    private ActionBarDrawerToggle mActionBarDrawerToggle;

    private Toolbar toolbar;

    private TabLayout mTabs;

    private ArrayList<View> viewList = viewList = new ArrayList<View>();
    private View v1,v2;
    private ViewPager mViewPager;

    private TableLayout tl_case_list;
    private TableRow tr_case_list;
    int status1 = 0, status2 = 0;

    Bundle bundle;

    preload load = new preload();
    DatabaseHelper DB_Helper = new DatabaseHelper(this);
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    //private SwipeRefreshLayout mSwipeRefreshLayout;
    //Search bar 需要優化。如果點擊旁邊，需要取消輸入。
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        //initial
        initToolbar();
        initDrawer();
        initDrawerList();
        initViewPager();

        //load.page_personal(0);
        /**bundle = this.getIntent().getExtras();
        if(bundle != null) {
            String url = bundle.getString("url");
            load.page_personal(url);
        }**/
        //load.page_personal("https://google.com.tw");
        // initSwipRefresh();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

    }

    /**
     * private void initSwipRefresh(){
     * mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
     * mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
     * //@Override
     * public void onRefresh() {
     * mSwipeRefreshLayout.setRefreshing(false);
     * }
     * });
     * <p/>
     * }
     */

    //導覽列
    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Set an OnMenuItemClickListener to handle menu item clicks
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Handle the menu item
                return true;
            }
        });
        // Inflate a menu to be displayed in the toolbar
        toolbar.inflateMenu(R.menu.menu_homepage);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

    }

    //Fragement Page
    private void initViewPager() {
        mTabs = (TabLayout) findViewById(R.id.tabs);
        mTabs.addTab(mTabs.newTab().setText("靈感倉庫"));
        mTabs.addTab(mTabs.newTab().setText("點子平台"));
        //mTabs.addTab(mTabs.newTab().setText(""))

        final LayoutInflater mInflater = getLayoutInflater().from(this);
        v1 = mInflater.inflate(R.layout.pager_item1, null);
        v2 = mInflater.inflate(R.layout.pager_item2, null);

        viewList.add(v1);
        viewList.add(v2);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(new SamplePagerAdapter(viewList));
        //mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabs));
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:

                        if (status1 == 0) {
                            load.page_personal(0);
                            //load.page_personal("https://www.google.com");
                        } else if (status1 == 1) {
                            load.page_personal(0);
                        }
                        break;
                    case 1:

                        if (status2 == 0) {
                            load.page_platform();
                        } else if (status2 == 1) {
                        }
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        //Click Tab can change page.
        mTabs.setupWithViewPager(mViewPager);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Homepage Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.pusher.lesson_helper/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Homepage Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.pusher.lesson_helper/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    private class SamplePagerAdapter extends PagerAdapter {
        private List<View> mListViews;

        public SamplePagerAdapter(List<View> mListViews) {
            this.mListViews = mListViews;
        }

        @Override
        public int getCount() {
            return mListViews.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return obj == view;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            if (position == 0)
                return "靈感倉庫";
            else if (position == 1)
                return "點子平台";
            else
                return "測試平台";
        }

        /**
         * 因为pagerAdapter是默认预加载前后一张的，
         * 所以当加载第一页时，调用了两次instantiateItem方法。
         * 第一次是加载本来的第一页，第二次是预加载第二页，所以log打印了0和1。
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            //View view = getLayoutInflater().inflate(R.layout.pager_item1, container, false);
            View view = mListViews.get(position);

            container.addView(view);
            if(status1 == 0){
                status1 = 1;
                load.page_personal(0);
            }

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

    }


    //左側選單
    private void initDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open, R.string.close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        mActionBarDrawerToggle.syncState();
        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
    }

    private void initDrawerList() {
        //需再設置layout。此側邊菜單，預定功能為老師個人會員頁面與倉庫設定等。
        listDrawer = (ListView) findViewById(R.id.left_drawer);
        //String[] drawer_menu = this.getResources().getStringArray(R.array.drawer_menu);
        String[] drawer_menu = this.getResources().getStringArray(R.array.drawer_menu);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.drawer_list_item, drawer_menu);
        listDrawer.setAdapter(adapter);
    }


    private MenuItem menuSearchItem = null;
    private MenuItem menuShareItem = null;

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_homepage, menu);

        menuSearchItem = menu.findItem(R.id.my_search);
        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        //SearchView searchView = (SearchView) menuSearchItem.getActionView();
        SearchView searchView = (SearchView) menuSearchItem.getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true); // Do not iconify the widget; expand it by default


        menuShareItem = menu.findItem(R.id.action_share);
        ShareActivity share = new ShareActivity(menuShareItem);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_about:
                Toast.makeText(this, "This is about", Toast.LENGTH_SHORT).show();
                // About option clicked.
                return true;
            case R.id.action_exit:
                // Exit option clicked.
                android.os.Process.killProcess(android.os.Process.myPid());
                return true;
            case R.id.action_settings:
                // Settings option clicked.
                Toast.makeText(this, "This is setting.", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_share:
                ShareActivity share = new ShareActivity(item);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public class preload {
        /** layout設定！ */
        TableRow.LayoutParams lp = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);

        LinearLayout.LayoutParams chiledParams3 = new LinearLayout.LayoutParams(0,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams chiledParams = new LinearLayout.LayoutParams(0,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout.LayoutParams chiledParams2 = new LinearLayout.LayoutParams(0,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        //靈感倉庫頁面loading
        public void page_personal(int id) {
            status1 = 1;  //想想如何解決重複載入的問題
            tl_case_list = (TableLayout) findViewById(R.id.case_list1);
            if(status1 == 1){
                tl_case_list.removeAllViews();
            }
            tl_case_list.setStretchAllColumns(true);
            //tl_case_list.setStretchAllColumns(true);
            /**TableRow tr_case_list = new TableRow(mViewPager.getContext());
            tr_case_list.setGravity(Gravity.CENTER_HORIZONTAL);

            TextView user_acc = new TextView(mViewPager.getContext());
            ImageButton edit = new ImageButton(mViewPager.getContext());
            edit.setImageResource(R.drawable.messaging_quote_icon);
            edit.setId(id);
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mViewPager.getContext(), "This will be set to edit."+v.getId(), Toast.LENGTH_SHORT).show();
                }
            });**/

            //new case save into SQLite database, and query, list them by tablerow dynamically.


            ArrayList<Case> cases = new ArrayList<Case>();
            cases = DB_Helper.get_AllCase_num();
            for (int i = 0; i < cases.size(); i++) {
                TableRow tr_case_list = new TableRow(mViewPager.getContext());
                tr_case_list.setGravity(Gravity.CENTER_HORIZONTAL);

                TextView title = new TextView(mViewPager.getContext());
                final String case_title = cases.get(i).getCaseTitle();
                final int case_id = cases.get(i).getID();
                TextView tag = new TextView(mViewPager.getContext());
                ImageButton edit = new ImageButton(mViewPager.getContext());
                edit.setImageResource(R.drawable.messaging_quote_icon);
                edit.setId(cases.get(i).getID());

                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(mViewPager.getContext(), "This will be set to edit.", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent();
                        Bundle bundle = new Bundle();
                        intent.setClass(Homepage.this,edit_note.class);
                        bundle.putString("Title", case_title);
                        bundle.putInt("ID", case_id);
                        intent.putExtras(bundle);
                        startActivity(intent);

                    }
                });

                /**
                 * 未來將「新增」，設置成浮動按鈕。
                 * FloatingActionButton add_case = (FloatingActionButton)findViewById(R.id.fab_new);
                 add_case.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                //write function to let user add personal case.
                }
                });
                 **/

                title.setText(cases.get(i).getCaseTitle());
                title.setTextSize(20);
                tag.setText(cases.get(i).getCaseTag());
                tag.setTextSize(10);
                tag.setTextColor(Color.GREEN);
                //user_acc.setLayoutParams(view_layout);
                tr_case_list.addView(title);
                tr_case_list.addView(tag);
                tr_case_list.addView(edit);


                tl_case_list.addView(tr_case_list);
            }
        }

        public void page_personal(String url) {

            tl_case_list = (TableLayout) findViewById(R.id.case_list1);
            //tl_case_list.setStretchAllColumns(true);
            TableRow tr_case_list = new TableRow(mViewPager.getContext());
            tr_case_list.setGravity(Gravity.CENTER_HORIZONTAL);

            //TextView user_acc = new TextView(mViewPager.getContext());
            Preview web_preview = new Preview(mViewPager.getContext());
            ImageButton edit = new ImageButton(mViewPager.getContext());
            edit.setImageResource(R.drawable.messaging_quote_icon);
            //edit.setId(id);
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mViewPager.getContext(), "This will be set to edit." + v.getId(), Toast.LENGTH_SHORT).show();
                }
            });

            //user_acc.setText("Test" + id);
            //user_acc.setTextSize(20);
            //user_acc.setLayoutParams(view_layout);
            web_preview.setData("https://google.com.tw");
            tr_case_list.addView(web_preview);
            tr_case_list.addView(edit);


            tl_case_list.addView(tr_case_list);

        }

        //點子平台的頁面loading
        public void page_platform() {
            status2 = 1;
            tl_case_list = (TableLayout) findViewById(R.id.case_list2);
            tl_case_list.setStretchAllColumns(true);


            //new case save into SQLite database, and query, list them by tablerow dynamically.
            for (int i=0;i<=10;i++) {
                int id[] = new int[11];
                id[i] = i;

                TableRow tr_case_list = new TableRow(mViewPager.getContext());
                LinearLayout layout = new LinearLayout(mViewPager.getContext());
                tr_case_list.setGravity(Gravity.CENTER_HORIZONTAL);
                //tr_case_list.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,10));

                //tr_case_list.setLayoutParams(lp);

                //lp = new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT);
                lp.weight = 1;
                //設置有Border的效果
                lp.setMargins(0, 0, 0, 10);
                layout.setLayoutParams(lp);
                layout.setWeightSum(10);


                chiledParams3.weight = 2;
                ImageButton imbtn_love = new ImageButton(mViewPager.getContext());
                imbtn_love.setImageResource(R.drawable.hearts);
                imbtn_love.setLayoutParams(chiledParams3);
                imbtn_love.setBackgroundColor(Color.alpha(0));
                imbtn_love.setId(id[i]);
                imbtn_love.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(mViewPager.getContext(),"press this is mean you like.",Toast.LENGTH_SHORT).show();
                    }
                });


                chiledParams.weight = 6;
                TextView user_acc = new TextView(mViewPager.getContext());
                user_acc.setText("Test" + i);
                user_acc.setGravity(Gravity.CENTER);
                user_acc.setTextSize(20);
                user_acc.setLayoutParams(chiledParams);
                user_acc.setId(id[i]);


                chiledParams2.weight = 2;
                ImageButton imbtn_ideasave = new ImageButton(mViewPager.getContext());
                imbtn_ideasave.setImageResource(R.drawable.idea);
                imbtn_ideasave.setLayoutParams(chiledParams2);
                imbtn_ideasave.setBackgroundColor(Color.alpha(0));
                imbtn_ideasave.setId(id[i]);
                imbtn_ideasave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(mViewPager.getContext(), "press this can add in 倉庫"+v.getId(), Toast.LENGTH_SHORT).show();
                        //page_personal(v.getId());
                        DB_Helper.add_case("班級經營",v.getId()+"'s case");

                    }
                });

                layout.addView(user_acc);
                layout.addView(imbtn_love);
                layout.addView(imbtn_ideasave);

                /**tr_case_list.addView(imbtn_love);
                tr_case_list.addView(user_acc);
                tr_case_list.addView(imbtn_ideasave);**/
                tr_case_list.addView(layout);
                tr_case_list.setBackgroundColor(Color.rgb(153,255,153));
                layout.setBackgroundColor(Color.rgb(224, 255, 255));

                tl_case_list.addView(tr_case_list);

                //Log.e("platform", "" + i);
            }

        }
    }

}
